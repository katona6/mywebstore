<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%--
	file: dbconfig.jsp
	desc: DB Connection & Product Listing page of myWebStore
	by:   Katona Tamas
	ver:  0.01
--%>

	<%
		java.sql.Connection connection = null;
		String url = "jdbc:mysql://localhost:3306/";
		String database = "katona6";
		String Driver = "com.mysql.jdbc.Driver";
		String username = "root";
		String password = "";
		
		
		//Connect to MySQL Database
			try{
				Class.forName(Driver);
				connection = DriverManager.getConnection(url+database, username, password);
				
			}catch(Exception e){
				e.printStackTrace();
			}
		
			if (connection==null){
				out.println("Failed to Connect DB.");
			}
			
		// Connected to Database
	    
		//Database Query to List products -->
		try{
			int sumcount=0;
			Statement statement;
			String query = "select * from Ital";
			statement = connection.createStatement();
			ResultSet resultset = statement.executeQuery(query);
	
			
			out.println("<table cellspacing='2' cellpadding='10' align='center'>");
			out.println("<tr>  <th>VONALKOD</th>  <th>NEV</th>  <th>GYARTO</th> <th>AR</th> <th>ALKOHOL</th> <th>KISZERELES</th> <th>SZARMAZASI_HELY</th>           </tr>");
			
			while(resultset.next()){
		
				out.println("<tr bgcolor='lightyellow'>	<td>" + resultset.getString(1) + "</td>        ");
				out.println("   						<td>" + resultset.getString(2) + "</td>        ");
				out.println("    						<td>" + resultset.getString(3) + "</td>        ");
				out.println("    						<td>" + resultset.getInt(4)    + " HUF</td>    ");
				out.println("    						<td>" + resultset.getInt(5)    + " %</td>      ");
				out.println("    						<td>" + resultset.getFloat(6)  + " liter</td>  ");
				out.println("	 						<td>" + resultset.getString(7) + "</td>        ");
				out.println("<td> <input type='submit' name='submitbutton' value='Kos�rba biggyeszt' onClick=\"document.cartForm.storeID.value='"+ resultset.getString(1)  +"'; document.cartForm.storeName.value='"+ resultset.getString(2)  +"'; document.cartForm.storePrice.value='"+ resultset.getInt(4)  +"';\" >   </td></tr>");
				
				sumcount++;
			}
			connection.close();
			out.println("</table>");
			out.println("<br> �sszesen "+ sumcount +" f�le italunk van");
		
		}catch(Exception e){
			e.printStackTrace();
		}

	%>