<%@page import="myWebStore.CartList"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.Random"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%!
	ArrayList<String> CartList = new ArrayList<String>();
	java.sql.Connection connection = null;
	String url = "jdbc:mysql://localhost:3306/";
	String database = "katona6";
	String Driver = "com.mysql.jdbc.Driver";
	String username = "root";
	String password = "";
	//ArrayList<CartList> cartlist = new ArrayList<CartList>();
	
%>
    

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%--
	file: cart.jsp
	desc: The Shopping Cart page
	by:   Katona Tamas
	ver:  0.01
--%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Kos�r</title>
	</head>
	<body background="fa.jpg">
		<center><img alt="Shopping Cart Picture Place Here" src="shopping_cart.png"></center> <br>
		Ez itten a kosaranty�. <br>
		
	<%
			
		String productID = request.getParameter("storeID");
		String productName = request.getParameter("storeName");
		String productPrice = request.getParameter("storePrice");
		String removecartitem = request.getParameter("removecartitem");
		Random generator = new Random();
		int generatedInt =0;
		
		//Connect to MySQL Database
		try{
			Class.forName(Driver);
			connection = DriverManager.getConnection(url+database, username, password);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		// Connected to Database
		
		
		// REMOVE from CART -->
		if (removecartitem != null){
			//CartList.remove(removecartitem);
			out.println("A '<u>" + removecartitem +"</u>' vonalk�d� term�k t�r�lve a Kos�rb�l! <br>");
		}
		
		// ADD to CART -->
		if (productID != null){
			
			generatedInt = generator.nextInt(5000);
			session.setAttribute("item_"+Integer.toString(generatedInt), productID);
			
			out.println("A(z) '<u>"+ productID +"</u>' vonalk�d� term�k beker�lt a Kos�rba. <br>");
			
		}

		
		
		// Display Session Attributes -->
		if (session.getAttributeNames().hasMoreElements() == false){
			out.println("A Kos�r �res!");
		}else{
			out.println("A kos�r tartalma: <br>");
			int sumtotal = 0; // Total Price to Pay
			//out.println(session.getAttribute("CartList"));						
			
			//DISPLAYING... -->
			for (Enumeration e = session.getAttributeNames(); e.hasMoreElements(); ) {
				String attribName = (String) e.nextElement();
				Object attribValue = session.getAttribute(attribName);
				
				// DB Query For Display The ID's "Name" and "Price" -->
				Statement statement;
				String query = "select * from Ital WHERE Vonalkod='"+attribValue+"'";
				statement = connection.createStatement();
				ResultSet resultset = statement.executeQuery(query);
				
				while(resultset.next()){
					out.println("<i>"+resultset.getString(2)+"</i>, Vonalkod: "+attribValue+", "+resultset.getInt(4) + " HUF");
					out.println("<a href=removeAttribute.jsp?name=" +attribName+"&value="+attribValue+" > [x] </a> <br>");
					sumtotal = sumtotal + resultset.getInt(4);
				}
			}
			out.println("<br> Fizetend� �sszesen: <u><b>"+ sumtotal + "</b></u> HUF");
						
		}
		connection.close();
		out.println("<br>");
		out.println("<br>");
		out.println("<a href=index.jsp>Vissza a F�oldalra</a> |");
		out.println("<a href=# onClick=\" var key=confirm('Biztos ezeket akarod venni?');  if(key==true){location='order.jsp';    }else{location='cart.jsp';}        \"     >Tov�bb a megrendel�shez>> </a>");
		
	
		
	%>
	</body>
</html>