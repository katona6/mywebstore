<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>myWebStore Admin</title>
	</head>
	
	<body background="fa.jpg">
	<%
		String loginName = request.getParameter("name");
		String loginPassword = request.getParameter("password");
		
		java.sql.Connection connection = null;
		String url = "jdbc:mysql://localhost:3306/";
		String database = "katona6";
		String Driver = "com.mysql.jdbc.Driver";
		String username = "root";
		String password = "";
		
		if( (loginName.equals("root1") && loginPassword.equals("root1") ) ||  (loginName.equals("root2") && loginPassword.equals("root2") )    ){
			out.println("Bejelentkezve: "+ loginName+"<br>");
			out.println("<center> Az alábbi megrendelések érkeztek</center>");
			
			//Connect to MySQL Database
			try{
				Class.forName(Driver);
				connection = DriverManager.getConnection(url+database, username, password);
				
				Statement statement = connection.createStatement();
				ResultSet resultset = statement.executeQuery("select * from orders");
				out.println("<table border='1' cellspacing='2' cellpadding='10' align='center'>");
				out.println("<tr>  <th>ID</th>  <th>Vonalkod</th>  <th>Termék neve</th> <th>Ár</th> <th>Megrendelö</th> <th>Elérhetöség</th> <th>Fizetési Eszköz</th> <th>Szállitási cim</th>       </tr>");
				while(resultset.next()){
					
					out.println("<tr><td>" + resultset.getInt(1) + "</td>        ");
					out.println("   						<td>" + resultset.getString(2) + "</td>         ");
					out.println("    						<td>" + resultset.getString(3) + "</td>         ");
					out.println("    						<td>" + resultset.getInt(4)    + "</td>			");
					out.println("    						<td>" + resultset.getString(5) +"</td>			");
					out.println("    						<td>" + resultset.getString(6)  + "</td>		");
					out.println("	 						<td>" + resultset.getString(7) + "</td>			");
					out.println("	 						<td>" + resultset.getString(8) + "</td></tr>	");
					
					
				}
				
				connection.close();
				out.println("</table>");
				
			}catch(Exception e){
				e.printStackTrace();
			}
		
			if (connection==null){
				out.println("Failed to Connect DB. <br>" );
			}
			out.println("<a href='index.jsp'>Vissza</a>    ");
			
			
		}else{
			
			out.println("Sikertelen Adminisztrátori Bejelentkezés! <br>");
			out.println("<a href='login.jsp'>Vissza</a>    ");
			
		}
	%>
	
	</body>
</html>